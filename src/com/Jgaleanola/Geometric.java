package com.sigmotoa;

public abstract class Geometric {

    public String figurename;
    public int sidesnumber;
    public double side,side2,side3,base,high,rad,apothem;
    public static double area,volume;
    public static double perimeter;

    public Geometric(String figurename, int sidesnumber, double side) {
        this.figurename = figurename;
        this.sidesnumber = sidesnumber;
        this.side = side;

    }

    public abstract void areaCalculation();
    public abstract void perimeterCalculation();
    public abstract void volume();
}
