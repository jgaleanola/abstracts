package com.sigmotoa;

public class Pentagon extends Geometric
{

    public Pentagon(double side,double apothem,double high) {
        super("Pentagon", 5, side);
    }

    @Override
    public void areaCalculation() {
        area= (perimeter*apothem)/2;
    }

    @Override
    public void perimeterCalculation() {
        perimeter = 5*side;
    }

    @Override
    public void volume() {
        area = (5*side*apothem)/2 * high;
    }
}
