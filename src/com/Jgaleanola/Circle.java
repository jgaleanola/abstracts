package com.sigmotoa;

public class Circle extends Geometric {
    public Circle(double rad)
    {
        super("Circle", 0, rad);
    }

    @Override
    public void areaCalculation()
    {
        area = Math.PI * Math.pow(rad,2);
    }

    @Override
    public void perimeterCalculation() {
        perimeter = (2*Math.PI*Math.pow(rad,2));
    }

    @Override
    public void volume() {
        volume = (4/3*Math.PI*Math.pow(rad,3));
    }
}
