package com.sigmotoa;

public class Triangle extends Geometric {
    public Triangle(double side,double side2,double side3,double base,double high) {
        super("Triangle", 3,side);
    }

    @Override
    public void areaCalculation()
    {
        area = (this.base * this.high)/2;
    }

    @Override
    public void perimeterCalculation()
    {
        perimeter = (this.side + this.side2+this.side3);
    }
    @Override
    public void volume()
    {
        volume = (1/3*(base*high));
    }
}
